/**
  *   Aufgabe:  Recherechieren Sie im Internet !
  * 
  *   Sie d�rfen nicht die Namen der Variablen ver�ndern !!!
  *
  *   Vergessen Sie nicht den richtigen Datentyp !!
  *
  *
  * @version 1.0 from 21.08.2019
  * @author << Ihr Name >>
  */

public class WeltDerZahlen {

  public static void main(String[] args) {
    
    /*  *********************************************************
    
         Zuerst werden die Variablen mit den Werten festgelegt!
    
    *********************************************************** */
    // Im Internet gefunden ?
    // Die Anzahl der Planeten in unserem Sonnesystem                    
    int anzahlPlaneten = 8;
    
    // Anzahl der Sterne in unserer Milchstra�e
    long anzahlSterne = 800000000000l;
    
    // Wie viele Einwohner hat Berlin?
    int bewohnerBerlin = 4500000;
    
    // Wie alt bist du?  Wie viele Tage sind das?
    short alterTage = 8313;
    
    // Wie viel wiegt das schwerste Tier der Welt?
    // Schreiben Sie das Gewicht in Kilogramm auf!
    int gewichtKilogramm = 150000;
    
    // Schreiben Sie auf, wie viele km� das gr��te Land er Erde hat?
    int flaecheGroessteLand = 1798242;
    
    // Wie gro� ist das kleinste Land der Erde?
    
    String flaecheKleinsteLand = "0,44km�";
    
    
    
    
    /*  *********************************************************
    
         Programmieren Sie jetzt die Ausgaben der Werte in den Variablen
    
    ***********************************************************  */
    
    System.out.println("Anzahl der Planeten: " + anzahlPlaneten);
    
    System.out.println("Anzahl der Sterne: " + anzahlSterne);
    
    System.out.println("Bewohner Berlins: " + bewohnerBerlin);
    
    System.out.println("Wie alt bin ich in Tagen?:  " + alterTage);
    
    System.out.println("Das schwerste Tier der Welt in KG: " + gewichtKilogramm);
    
    System.out.println("Fl�che vom gr��ten Land: " + flaecheGroessteLand);
    
    System.out.println("Fl�che vom kleinsten Land: " + flaecheKleinsteLand);
    
    
    
    
    System.out.println(" *******  Ende des Programms  ******* ");
    
  }
}

