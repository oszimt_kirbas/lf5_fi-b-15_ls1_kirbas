﻿import java.util.Scanner;

class Fahrkartenautomat
{
    public static void main(String[] args)
    {
       Scanner tastatur = new Scanner(System.in);
      


       
       fahrkartenBezahlen();
       fahrkartenAusgeben();
       rueckgeldAusgeben();


    }
       public static double fahrkartenbestellungErfassen() { 
           int ticket = 0;	//Hier habe ich mich doch für int entschieden, da es der Datentyp für Ganzzahlen ist, und man Tickets nur im Ganzen kaufen kann.
           double kartenpreis; //Hier habe ich einen Double benutzt, da dann auch die Gleitkommastellen mit einbezogen werden können
           double zuZahlenderBetrag;
         
          	Scanner tastatur = new Scanner(System.in);
              System.out.print("Ticketpreis: ");
              kartenpreis = tastatur.nextDouble();
              System.out.print("Anzahl der Tickets: ");
              ticket = tastatur.nextInt();
              zuZahlenderBetrag = ticket * kartenpreis;
              System.out.print("Zu zahlender Betrag (EURO): ");
              System.out.printf ("%.2f\n\n",zuZahlenderBetrag);
              
              return zuZahlenderBetrag;
}
       
       public static double fahrkartenBezahlen() {
           double zuZahlenderBetrag = fahrkartenbestellungErfassen();;
           double eingezahlterGesamtbetrag;
           eingezahlterGesamtbetrag = 0.00;
           double eingeworfeneMünze = 0;

        	 Scanner eingabe= new Scanner(System.in);
             while(eingezahlterGesamtbetrag < zuZahlenderBetrag)
             {
          	   
          	   System.out.println("Noch zu zahlen: ");
          	   System.out.printf ("%.2f",(zuZahlenderBetrag - eingezahlterGesamtbetrag));
          	   System.out.println(" Euro");
          	   System.out.print("Eingabe (mind. 5Ct, höchstens 2 Euro): ");
          	   eingeworfeneMünze = eingabe.nextDouble();
               eingezahlterGesamtbetrag += eingeworfeneMünze;
             }
             return eingezahlterGesamtbetrag;
       }

    public static double rueckgeldAusgeben() {
    	  double rückgabebetrag;
    	  double eingezahlterGesamtbetrag = fahrkartenBezahlen();
    	  double zuZahlenderBetrag = fahrkartenbestellungErfassen();
    	  rückgabebetrag = eingezahlterGesamtbetrag - zuZahlenderBetrag;
    if(rückgabebetrag > 0.0)
    {
 	   System.out.println("Der Rückgabebetrag in Höhe von " + rückgabebetrag + " EURO");
 	   System.out.println("wird in folgenden Münzen ausgezahlt:");

        while(rückgabebetrag >= 2.0) // 2 EURO-Münzen
        {
     	  System.out.println("2 EURO");
	          rückgabebetrag -= 2.0;
        }
        while(rückgabebetrag >= 1.0) // 1 EURO-Münzen
        {
     	  System.out.println("1 EURO");
	          rückgabebetrag -= 1.0;
        }
        while(rückgabebetrag >= 0.5) // 50 CENT-Münzen
        {
     	  System.out.println("50 CENT");
	          rückgabebetrag -= 0.5;
        }
        while(rückgabebetrag >= 0.2) // 20 CENT-Münzen
        {
     	  System.out.println("20 CENT");
	          rückgabebetrag -= 0.2;
        }
        while(rückgabebetrag >= 0.1) // 10 CENT-Münzen
        {
     	  System.out.println("10 CENT");
	          rückgabebetrag -= 0.1;
        }
        while(rückgabebetrag >= 0.05)// 5 CENT-Münzen
        {
     	  System.out.println("5 CENT");
	          rückgabebetrag -= 0.05;
        }
    }
	return rückgabebetrag;
	}
    public static void fahrkartenAusgeben() {
  	 System.out.println("\nFahrschein wird ausgegeben");
       for (int i = 0; i < 8; i++)
       {
          System.out.print("=");
          try {
			Thread.sleep(250);
				} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    }
    System.out.println("\nVergessen Sie nicht, den Fahrschein\n"+
               "vor Fahrtantritt entwerten zu lassen!\n"+
               "Wir wünschen Ihnen eine gute Fahrt.");
    System.out.println("\n\n");
   }
}