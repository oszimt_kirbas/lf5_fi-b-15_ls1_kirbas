
public class Temperaturtabelle {

	public static void main(String[] args) {
		String c = "Celsius";
		String f = "Fahrenheit";
		
		System.out.printf("%-12s| %10s\n", f, c);  //Nachdem ich die Strings vordefiniert habe, habe ich sie hier einfach nur eingesetzt.
		System.out.printf("%-24s\n", "------------------------" );
		System.out.printf("%-12d| %10.2f\n", -20, -28.8889);   //Hier habe ich die Werte mit einem Float (f) gekennzeichnet und mit .2 auf die ersten 2 Nachkommastellen reduziert.
		System.out.printf("%-12d| %10.2f\n", -10, -23.3333);
		System.out.printf("%-12d| %10.2f\n", +0, -17.7778);
		System.out.printf("%-12d| %10.2f\n", +20, -6.6667);
		System.out.printf("%-12d| %10.2f\n", +30, -1.1111);
	}

}
